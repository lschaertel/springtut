package hello;

import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {
    @RequestMapping("/")
    public String index()
    {
        return "Greetings from Spring boot!";
    }
    @RequestMapping("/t")
    public String test()
    {
        return "Des is a test";
    }
}
